## Registers

All registers and instructions are 16-bit.

```
r0,..,r7
pc
```

### Instructions and encoding

Types are R, I, L, S, J

```
R [ op ][ rd ][ rs1 ][ rs2 ][ 4x ]
I [ op ][ rd ][ imm ]
L [ op ][ rd ][ 4x ][ mem ]
S [ op ][ 3x ][ rs1 ][ 1x ][ mem ]
J [ op ][ addr ][ rs1 ][ addr ]
```

```
op   := 15:13
rd   := 12:10
rs1  := 9:7
rs2  := 6:4
imm  := 9:0
mem  := 5:0
addr := 12:10,6:0
```

```
add 000
sub 001

lw  010
sw  011
li  100

jeq 101
jlt 110
jgt 111
```
