// vim:ft=verilog

module FullAdder
  ( input        a, b
  , input  [1:0] ci
  , output [1:0] s, co
  );

  wire p, g, e;

  assign p = a ^ b; // propagate carry
  assign g = a & b; // generate carry
  assign e = |ci;   // not empty

  // if ci is empty, then s is empty
  // otherwise, s = p ^ ci
  assign s = ({p, p} ^ ci) & {e, e};

  // if p, then propagate the carry
  // otherwise, the carry is g (generate carry).
  // g is converted to dual rail
  assign co = p ? ci : {g, ~g};
endmodule

module Adder
 #( parameter XLEN = 8
 )( input  [XLEN-1:0] a, b
  , input             ci
  , output [XLEN-1:0] s
  , output            co

  , input  i_req
  , output o_req
  );

    (* KEEP = "yes" *) wire [XLEN*2 - 1:0] cos, ss;
    (* KEEP = "yes" *) wire [XLEN - 1:0] valid; // values non empty

    assign co = cos[XLEN*2 - 2 +: 2] == 2'b10;

    // Only propagate req when outputs are valid
    assign o_req = (&valid) & |cos[XLEN*2 - 2 +: 2];

    genvar i;
    for (i = 0; i < XLEN; i = i + 1) begin
        assign s[i] = ss[i*2 +: 2] == 2'b10;
        assign valid[i] = |ss[i*2 +: 2];

        if (i == 0)
            FullAdder fa (
                .a(a[i]), .b(b[i]), .s(ss[i*2 +: 2]),
                .co(cos[i*2 +: 2]),

                // if ~i_req, then empty,
                // otherwise, equal to ci
                // (converts ci to dual rail)
                .ci({ci, ~ci} & {i_req, i_req})
            );
        else
            FullAdder fa (
                .a(a[i]), .b(b[i]), .s(ss[i*2 +: 2]),
                .co(cos[i*2 +: 2]),
                .ci(cos[(i-1)*2 +: 2])
            );
        end
endmodule
