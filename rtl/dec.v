`include "delay.v"

module Dec
 #( parameter DELAY = 0
 )( input [15:0] inst
  , output [2:0] rd, rs1, rs2
  , output [5:0] mem_addr
  , output [9:0] jmp_addr
  , output [15:0] imm

  , output rd_w, mem_w
  , output is_sub
  , output is_jeq, is_jlt, is_jgt
  , output rd_alu, rd_mem

  , input i_req, o_ack
  , output o_req, i_ack
  );

    assign i_ack = o_ack;
    Delay #(DELAY) d (o_req, i_req);

    assign rd = inst[12:10];
    assign rs1 = inst[9:7];
    assign rs2 = inst[6:4];
    assign imm = {6'b0, inst[9:0]};
    assign mem_addr = inst[5:0];
    assign jmp_addr = {inst[12:10], inst[6:0]};

    assign is_jeq = op_jeq;
    assign is_jlt = op_jlt;
    assign is_jgt = op_jgt;

    assign is_sub = op_sub;

    assign rd_w = |{
        op_add, op_sub,
        op_li, op_lw
    };

    assign mem_w = op_sw;

    assign rd_alu = op_add | op_sub;
    assign rd_mem = op_lw;
    // otherwise, imm

    wire [2:0] op = inst[15:13];

    wire
        op_add = op == 3'b000,
        op_sub = op == 3'b001,
        op_lw  = op == 3'b010,
        op_sw  = op == 3'b011,
        op_li  = op == 3'b100,
        op_jeq = op == 3'b101,
        op_jlt = op == 3'b110,
        op_jgt = op == 3'b111;
endmodule
