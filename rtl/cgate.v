`ifndef cgate
`define cgate

module CGate (input rst, a, b, output y);
    assign y = !rst & ((a & b) | ((a | b) & y));
endmodule

module CBlock (input rst, i_req, o_ack, output i_ack);
    wire a, b;
    assign a = i_req, b = ~o_ack;

    assign i_ack = !rst & ((a & b) | ((a | b) & i_ack));
endmodule

`endif
