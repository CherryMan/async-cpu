`include "cgate.v"
`include "delay.v"

module RegFile
 #( parameter XLEN = 16
  , parameter RLEN = 3 // bits for register address
  , parameter DELAY = 0
 )( input [RLEN-1:0] rs1, rs2, rd // dest register is rd
  , input rd_w, rst
  , input [XLEN-1:0] rd_i
  , output [XLEN-1:0] rs1_o, rs2_o

  , input  i_r_req, o_r_ack
  , output o_r_req, i_r_ack

  , input  i_w_req, o_w_ack
  , output o_w_req, i_w_ack
  );

    (* ASYNC_REG = "TRUE" *) // xilinx: keep register
    reg [XLEN-1:0] regs [2**RLEN - 1:0];

    assign rs1_o = regs[rs1];
    assign rs2_o = regs[rs2];

    CBlock c (
        .rst(rst),
        .i_req(i_w_req),
        .o_ack(o_w_ack),
        .i_ack(i_w_ack)
    );

    Delay #(DELAY) dw (o_w_req, i_w_ack);

    assign i_r_ack = o_r_ack;
    Delay #(DELAY) dr (o_r_req, i_r_req);

    always @(posedge i_w_ack)
        if (rd_w) regs[rd] <= rd_i;
endmodule
