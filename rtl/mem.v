`include "cgate.v"
`include "delay.v"

module Mem
 #( parameter XLEN = 16
  , parameter ALEN = 6
  , parameter DELAY = 0
 )( input rst
  , input [XLEN-1:0] d_i
  , output [XLEN-1:0] d_o
  , input [ALEN-1:0] addr
  , input mem_w

  , input  [15:0] io_i
  , output [15:0] io_o

  , input i_req, o_ack
  , output o_req, i_ack
  );

    // use 1/2 of address space for register
    localparam MAX_ADDR = 2**(ALEN - 1);
    genvar i;

    (* ASYNC_REG = "TRUE" *)
    reg [XLEN-1:0] _d_o;

    assign d_o = _d_o;

    (* ASYNC_REG = "TRUE" *) // xilinx: keep reg
    reg [XLEN-1:0] regs [MAX_ADDR - 1:0];

    (* ASYNC_REG = "TRUE" *)
    reg [XLEN-1:0] io_regs [MAX_ADDR+16-1:MAX_ADDR];
    wire [XLEN-1:0] io [2**ALEN-1:MAX_ADDR];

    for (i = 0; i < 16; i = i + 1) begin
        assign io[i+MAX_ADDR] = io_regs[i+MAX_ADDR];

        assign io[i+16+MAX_ADDR] = {15'b0, io_i[i]};
        assign io_o[i] = |io[i+MAX_ADDR];
    end

    CBlock c (.rst(rst), .i_req(i_req), .o_ack(o_ack), .i_ack(i_ack));
    Delay #(DELAY) d (o_req, i_ack);

    always @(posedge i_ack) begin
        _d_o <= addr[ALEN-1] ? io[addr] : regs[addr];

        if (mem_w & ~addr[ALEN-1]) regs[addr] <= d_i;
        if (mem_w & addr[ALEN-1]) io_regs[addr] <= d_i;
    end
endmodule
