`timescale 1ns / 1ps

`include "vunit_defines.svh"
`include "delay.v"

module tb_Delay;
    logic i, o;
    Delay #(10) d (.*);

    `TEST_SUITE begin
        `TEST_CASE("behaviour") begin
            i = 0; #11 `CHECK_EQUAL(o, i);
            i = 1; #11 `CHECK_EQUAL(o, i);
            i = 0; #11 `CHECK_EQUAL(o, i);
            i = 1; #11 `CHECK_EQUAL(o, i);
        end
    end
endmodule
