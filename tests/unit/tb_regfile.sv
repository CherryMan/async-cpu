`timescale 1ns / 1ps

`include "vunit_defines.svh"
`include "regfile.v"

module tb_Regfile;
    parameter XLEN = 32;
    parameter RLEN = 5;

    logic [RLEN-1:0] rs1, rs2, rd;
    logic [XLEN-1:0] rd_i;
    logic rd_w, rst;
    logic i_r_req, i_w_req, o_w_ack;
    wire o_r_ack;

    wire [XLEN-1:0] rs1_o, rs2_o;
    wire o_r_req, i_r_ack, o_w_req, i_w_ack;

    integer n, m;

    assign o_r_ack = o_r_req;

    RegFile #(XLEN, RLEN) regs (.*);

    `TEST_SUITE begin
        `TEST_CASE_SETUP begin
            n = $urandom();
            m = $urandom();

            i_r_req = 0;
            i_w_req = 0;
            o_w_ack = 0;

            rd_w = 0;
            rst = 1;

            #1 rst = 0;
        end

        `TEST_CASE("behaviour") begin
            // write
            rd = 0;
            rd_i = n;
            rd_w = 1;

            i_w_req = 1;

            #1 wait(i_w_ack);
            `CHECK_EQUAL(o_w_req, 1);
            rd_w = 0;
            o_w_ack = 1;
            i_w_req = 0;

            // read
            rs1 = 0;
            rs2 = 0;
            i_r_req = 1;
            #1 wait(i_r_ack);
            #1 `CHECK_EQUAL(rs1_o, n);
               `CHECK_EQUAL(rs2_o, n);
        end
    end
endmodule
