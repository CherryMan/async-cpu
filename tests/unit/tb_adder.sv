`timescale 1ns / 1ps

`include "vunit_defines.svh"
`include "adder.v"

module tb_Adder;
    localparam XLEN = 32;

    logic [XLEN-1:0] a, b, s;
    logic ci, co, i_req, o_req;
    integer n;

    Adder #(XLEN) adder (.*);

    task test(integer x, y, bit carry);
        #1 i_req = 0;
        a = x;
        b = y;
        ci = carry;

        #1 i_req = 1;
        wait(o_req == 1);

        `CHECK_EQUAL({co, s}, x + y + carry);
    endtask

    `TEST_SUITE begin
        `TEST_CASE("behaviour") begin
            test(0, 0, 0);
            test(1, 0, 0);
            test(1, 1, 0);
            test(5, 5, 1);

            for (n = 0; n < 50; ++n) begin
                test($urandom(), $urandom(), 0);
                test($urandom(), $urandom(), 1);
            end
        end
    end
endmodule
