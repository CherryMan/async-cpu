`timescale 1ns / 1ps

`include "vunit_defines.svh"
`include "cgate.v"

module tb_CGate;
    logic a, b, y, rst;

    CGate c (.*);

    task test(logic v, w, res);
        a = v;
        b = w;
        #1 `CHECK_EQUAL(y, res);
    endtask

    `TEST_SUITE begin
        `TEST_CASE_SETUP begin
            a = 0;
            b = 0;
            rst = 0;
            #1;
        end

        `TEST_CASE("reset") begin
            rst = 1;
            #1 rst = 0;
            #1 `CHECK_EQUAL(y, 0);
        end

        `TEST_CASE("behaviour") begin
            test(0, 0, 0);
            test(1, 0, 0);
            test(0, 1, 0);
            test(1, 1, 1);
            test(1, 0, 1);
            test(0, 1, 1);
            test(0, 0, 0);
        end
    end
endmodule
