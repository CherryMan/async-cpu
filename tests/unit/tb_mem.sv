`timescale 1ns / 1ps

`include "vunit_defines.svh"
`include "mem.v"

module tb_Mem;
    parameter XLEN = 16;
    parameter ALEN = 6;
    parameter DELAY = 0;

    logic rst, mem_w;
    logic [ALEN-1:0] addr;
    logic [XLEN-1:0] d_i;
    logic i_req;
    logic [15:0] io_i;
    wire o_ack;

    wire [15:0] io_o;
    wire [XLEN-1:0] d_o;
    wire o_req, i_ack;

    assign o_ack = o_req;

    Mem #(XLEN, ALEN, DELAY) mem (.*);

    task set(bit [ALEN-1:0] a, bit [XLEN-1:0] data);
        addr = a;
        mem_w = 1;
        d_i = data;
        #1 i_req = 1;
        wait(i_ack);

        #1 i_req = 0;
        wait(~i_ack);
        mem_w = 0;
    endtask

    task check(bit [ALEN-1:0] a, bit [XLEN-1:0] res);
        addr = a;
        #1 i_req = 1;
        wait(i_ack);

        #1 `CHECK_EQUAL(d_o, res);

        #1 i_req = 0;
        wait(~i_ack);
    endtask

    `TEST_SUITE begin
        `TEST_CASE_SETUP begin
            i_req = 0;
            mem_w = 0;

            io_i = 16'b0000_0001_0001_0001;

            #1 rst = 1;
            #1 rst = 0;
        end

        `TEST_CASE("behaviour") begin
            // regular reads and writes
            set(0, 1);
            check(0, 1);

            // io input
            check(48, 1);
            check(49, 0);
            check(52, 1);

            // io output
            set(32, 1);
            set(33, 2);
            set(34, 0);
            `CHECK_EQUAL(io_o[0], 1);
            `CHECK_EQUAL(io_o[1], 1);
            `CHECK_EQUAL(io_o[2], 0);
        end
    end
endmodule
